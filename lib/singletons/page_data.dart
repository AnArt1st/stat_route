import 'package:stat_route/model/stat.dart';

class PageData {
  static final PageData _appData = new PageData._internal();

  Stat stat;
  factory PageData() {
    return _appData;
  }
  PageData._internal();
}

final pageData = PageData();
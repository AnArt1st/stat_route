import 'dart:math';

import 'package:flutter/material.dart';
import 'package:stat_route/model/stat.dart';
import 'package:stat_route/utils/background_scaffold.dart';
import 'package:stat_route/utils/user_utils.dart';
import 'stats_page.dart';
import 'package:stat_route/singletons/page_data.dart';

class StatRouteHome extends StatefulWidget {

  @override
  _StatRouteHomeState createState() {
    return new _StatRouteHomeState();
  }
}

class _StatRouteHomeState extends State<StatRouteHome> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  Future<void> executeAfterBuild() async {
    String welcomeMsg = 'Welcome to the new standard for sports stats';

    // this code will get executed after the build method
    // because of the way async functions are scheduled
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: shadowText(welcomeMsg, smallTextSize),

          backgroundColor: Colors.blueGrey[700],
          duration: Duration(seconds: 3),
        ));
  }

  @override
  void initState() {

    super.initState();
    Future.delayed(Duration(milliseconds: 0)).then(
            (_) => executeAfterBuild()
    );
  }

  static List<Stat> getStatEntries() {
    List<Stat> statEntries = new List<Stat>();
    List<String> names = <String>['John Doe', 'Peter Svidler', 'Robbert J Fischer', 'Michael Jordon'];
    for (String name in names) {
      Stat stat = new Stat();
      // Generate a number to SIMULATE an ID
      Random rand = new Random.secure();
      int randNumber = rand.nextInt(90000000);
      // Set stat
      stat.name = name;
      stat.title = 'Player of sports.';
      stat.statID = randNumber.toString();
      stat.wins = rand.nextInt(1000);
      // Add entry
      statEntries.add(stat);
    }
    return statEntries;
  }

  List<Stat> statEntries = getStatEntries();
//  List<String> statEntries = <String>['John Doe', 'Peter Svidler', 'Robbert J Fischer', 'Michael Jordon'];
  List<Color> colorArray = <Color>[Colors.grey[400], Colors.grey[500]];

  @override
  Widget build(BuildContext context) {
    var scaffold = scaffoldBackground(
        new Center(
      child: ListView.separated(
        padding: const EdgeInsets.all(8.0),
        itemCount: statEntries.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            child: Container(
              height: 50,
              color: colorArray[index%2],
              child: Center(child: Text('Name: ${statEntries[index].name}') ),
            ),
            onTap: () {
              /// Set stat and push to stats_page
              pageData.stat = statEntries[index];
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => StatsPage(),
                  ));
            },
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
    ), _scaffoldKey, 'images/stat_route_blurred_v3.png');


    return scaffold;
  }


}//end class

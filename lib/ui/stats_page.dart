import 'package:flutter/material.dart';
import 'package:stat_route/model/stat.dart';
import 'package:stat_route/utils/background_scaffold.dart';
import 'package:stat_route/utils/user_utils.dart';
import 'package:stat_route/singletons/page_data.dart';

class StatsPage extends StatefulWidget {

  @override
  _StatsPageState createState() {
    return new _StatsPageState();
  }
}

class _StatsPageState extends State<StatsPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();



  Future<void> executeAfterBuild() async {
    String welcomeMsg = 'Welcome to the new standard for sports stats';

    // this code will get executed after the build method
    // because of the way async functions are scheduled
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: shadowText('Showing stats for '+pageData.stat.name, smallTextSize),

          backgroundColor: Colors.blueGrey[700],
          duration: Duration(seconds: 3),
        ));
  }

  @override
  void initState() {

    super.initState();
    Future.delayed(Duration(milliseconds: 0)).then(
            (_) => executeAfterBuild()
    );
  }


  List<String> statDataList = <String>[
    'Name: ' + pageData.stat.name,
    'Title: ' + pageData.stat.title,
    'Player ID: ' + pageData.stat.statID,
    'Number of career wins' + pageData.stat.wins.toString()
  ];
  List<Color> colorArray = <Color>[Colors.grey[400], Colors.grey[500]];

  @override
  Widget build(BuildContext context) {
    var scaffold = scaffoldBackground(
        new Center(
          child: ListView.separated(
            padding: const EdgeInsets.all(8.0),
            itemCount: statDataList.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                child: Container(
                  height: 50,
                  color: colorArray[index%2],
                  child: Center(child: Text(statDataList[index])),
                ),
                onTap: () {
                  ///

                },
              );
            },
            separatorBuilder: (BuildContext context, int index) => const Divider(),
          ),
        ), _scaffoldKey, 'images/stat_route_blurred_v3.png');


    return scaffold;
  }


}//end class
import 'package:flutter/material.dart';
import 'package:stat_route/utils/user_utils.dart';
import '../utils/background_scaffold.dart';
import 'stat_route_home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stat Route',
      theme: ThemeData(

        primarySwatch: Colors.grey,
      ),
      home: MyHomePage(title: 'Stat Route Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // Fields in a Widget subclass are
  // always marked "final".
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  bool rememberMe = true; // Default value
  bool hidePassword = true;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    return scaffoldBackground(
        new Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              getTextField('Email', emailController, false),
              new SizedBox(height: 10.0,),
              getTextField('Password', passwordController, true),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  loginButton(context),
                  forgotPasswordButton(context)
                ],
              ),
              rememberMeCheckboxRow(),
            ],

          ), _scaffoldKey, 'images/stat_route_blurred_v2.png');
  }

  Widget rememberMeCheckboxRow(){
    return Padding(
      padding: const EdgeInsets.only(bottom: 30.0),
      child: new Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Checkbox(
                checkColor: Colors.white,
                value: rememberMe,
                onChanged: _onRememberMeChanged
            ),
          ),
          Text(
            "Remember Me",
            style: TextStyle(
                color: smallTextColor,
                fontSize: smallTextSize
            ),
          ),
        ],
      ),
    );
  }

  void _onRememberMeChanged(bool newValue) => setState(() {
    rememberMe = newValue;

    if (rememberMe) {
      print('Remember me.');
    } else {
      print('Forget me.');
    }
  });

  Widget loginButton(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 5.0, top: 20.0, bottom: 0.0),
        child: GestureDetector(
          onTap: () {
            /// Check email and password.
            if (emailController.text.length > 1 && passwordController.text.length > 7) {
              /// Find user. Then...
              checkIfFoundUser(emailController.text.trim(), passwordController.text);
            } else {
//              printUserNotFound();
              print('User not found.');
            }

          },
          child: buttonContainer(Colors.red, "Login", 20.0),
        ),
      ),
    );
  }

  Future<void> checkIfFoundUser(String email, String pass) async {

  // Check if user was found in database.
    if (emailController.text.length > 2) {

      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => StatRouteHome(),
          ));
    } else {
//      printUserNotFound();
      print('Invalid user.');
    }
  }

  Widget forgotPasswordButton(BuildContext context) {
    return Expanded(

      child: Padding(
        padding: const EdgeInsets.only(left: 10.0, right: 20.0, top: 20.0),
        child: GestureDetector(
          onTap: () {
            print('Send');
            if(emailController.text.isNotEmpty) {
              resetPassword(emailController.text);
            }

          },
          child: buttonContainer(Colors.black54, "Forgot Password", smallTextSize),
        ),
      ), // GestureDetector
    );
  }

  Future<void> resetPassword(String email) async {
    ///TODO
//    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
//    await _firebaseAuth.sendPasswordResetEmail(email: email);
    print('Sent email to reset password.');
  }

// Hidden adds obscure text button to field if true.
Widget getTextField(String label, TextEditingController control, bool hidden) {
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
    child: Container(
      decoration: new BoxDecoration(
          color: Colors.black38,
          borderRadius: new BorderRadius.circular(8.0)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: new TextField(
          style: new TextStyle(
            color: Colors.white,
          ),
          cursorColor: Colors.white,
          controller: control,
          obscureText: (hidePassword && hidden), // Obscure if true.
          decoration: new InputDecoration(
            labelStyle: new TextStyle(
              color: Colors.white70,
              fontWeight: FontWeight.bold,
            ),
            labelText: label,
            suffixIcon: suffixIconButton(hidden),
          ),
        ),
      ),
    ),
  );
}

Widget suffixIconButton(bool hidden) {
  if (hidden == false) return null;
  // Else add hide password button.
  return IconButton(
      icon: Icon(Icons.visibility),
      onPressed: () {
        if (hidePassword == true) {
          debugPrint('Show password');
          setState(() {
            hidePassword = false;
          });
        } else {
          debugPrint('Hide password');
          setState(() {
            hidePassword = true;
          });
        }

      });
}

} // End of class





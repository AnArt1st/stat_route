import 'package:jaguar_orm/jaguar_orm.dart';

class Stat {

  @PrimaryKey(auto: false, isNullable: false)
  String statID="";
  @Column(isNullable: true)
  String name=" ";
  @Column(isNullable: true)
  String title=" ";
  @Column(isNullable: true)
  int wins=0;

  Stat({this.statID, this.name, this.title, this.wins});

  static const String statIDName = 'statID';
  static const String nameName = 'name';
  static const String titleName = 'title';
  static const String winsName = 'wins';
}
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:sqflite/sqflite.dart';

import 'stat_bean.dart';


/// The adapter
SqfliteAdapter _statAdapter;
/// File name
String statFileName = '/statsFile.json';

class StatSqflite {

  StatBean bean;

  init() async {
    if(_statAdapter == null) {
      _statAdapter = new SqfliteAdapter(await getDatabasesPath()+statFileName, version: 1);
    }
    if(bean == null) {
      await _statAdapter.connect();
      bean = new StatBean();
    }
    await bean.createTable();
  }

  close() async {
    await _statAdapter.close();
  }

}
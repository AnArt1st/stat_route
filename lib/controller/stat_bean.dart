import 'dart:async';
import 'dart:convert';
import 'package:jaguar_orm/jaguar_orm.dart';
import 'package:jaguar_query_sqflite/jaguar_query_sqflite.dart';
import 'package:sqflite/sqflite.dart';
import 'package:stat_route/model/stat.dart';


/// The adapter
SqfliteAdapter _statAdapter;
String statFileName = '/statsFile.json';

class StatBean {
  /// DSL Fields
  final StrField statID = new StrField(Stat.statIDName);
  final StrField name = new StrField(Stat.nameName);
  final StrField title = new StrField(Stat.titleName);
  final IntField wins = new IntField(Stat.winsName);
  ///

  /// File name for the model this bean manages
  String statFileName = '/statsFile.json';
  /// Table name
  String statsTableName = 'stats_table';

  Future<Null> createTable() async {
    final statsTable = new Create(statsTableName, ifNotExists: true);

    await _statAdapter.createTable(statsTable);
  }

  /// Inserts a new user into table
//  Future insert(Stat stat) async {
//
//
//    final insert = Insert(statsTableName).setValues({
//      // Values
//    });
//
//    return await insert.exec(_statAdapter); //_adapter.insert(inserter);
//  }

//  Future<Upc> fetchUpcWithTitle(String title) async {
//
//
//  }

  /// Finds one post by [upcnumber]
//  Future<Upc> findOne(String upcnumber) async {
//    Find updater = new Find(upcTableName);
//
//    updater.where(this.upcnumber.eq(upcnumber));
//
//    Map upcMap = await _upcAdapter.findOne(updater);
//
//    Upc upc = await buildUpcFromMap(upcMap);
//
//
//    return upc;
//  }

  /// Deletes all posts
//  Future<int> removeAll() async {
//    Remove deleter = new Remove(upcTableName);
//
//    return await _upcAdapter.remove(deleter);
//  }

  /// Finds all users
//  Future<List<Upc>> findAll() async {
//    Find finder = new Find(upcTableName);
//
//    List<Map> maps = (await _upcAdapter.find(finder)).toList();
//
//    List<Upc> posts = new List<Upc>();
//
//    for (Map map in maps) {
//      Upc upc = await buildUpcFromMap(map);
//      posts.add(upc);
//    }
//
//    return posts;
//  }

  /// Deletes a user by [upcnumber] (upcnumber is unique)
//  Future<int> remove(String upcnumber) async {
//    Remove deleter = new Remove(upcTableName);
//
//    /// TODO only decrement count. Remove if zero left.
//    deleter.where(this.upcnumber.eq(upcnumber));
//
//    return await _upcAdapter.remove(deleter);
//  }

}
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

final double smallTextSize = 16.0;
final smallTextColor = Colors.black;
const int BIGNUM = 2147000000;



Widget myCustomAlert(BuildContext context, String title, String message){
  return AlertDialog(

    backgroundColor: Colors.black,
    contentTextStyle: TextStyle(
      fontWeight: FontWeight.bold,
    ),
    title: shadowText(title, 18.0),
    content: new Text(message),
    actions: <Widget>[
      // usually buttons at the bottom of the dialog
      new FlatButton(
        child: new Text("Close"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
    ],
  );
}

//Widget myBottomAppBar(BuildContext context, String helpTitle, String helpMessage){}

/// Returns text with a shadow.
Widget shadowText(String text, double fontSize){
  return Text(
    text,
    style: new TextStyle(
      fontSize: fontSize,
      color: Colors.redAccent[100],
      shadows: <Shadow>[
        Shadow(
          offset: Offset(2.0, 2.0),
          blurRadius: 3.0,
          color: Color.fromARGB(255, 0, 0, 0),
        ),
        Shadow(
          offset: Offset(2.0, 2.0),
          blurRadius: 2.0,
          color: Colors.black54,
        ),
      ],
    ),
  );
}

Widget myAppBar(){
  return AppBar(
    title: Container(
      child: new SizedBox(
        child: Image.asset(
          'images/SR_Logo.png',
          fit: BoxFit.fill,
        ),
        height: kBottomNavigationBarHeight - 10,
        width: 200.0,
      ),

    ),
      elevation: 9.0,
      iconTheme: new IconThemeData(
        color: Colors.red,),
  );
}




Widget getTextField(String label, TextEditingController control, bool hidden) {
  return Padding(
    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
    child: Container(
      decoration: new BoxDecoration(
          color: Colors.black38,
          borderRadius: new BorderRadius.circular(8.0)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: new TextField(
          style: new TextStyle(
            color: Colors.white,
          ),
          cursorColor: Colors.white,
          controller: control,
          obscureText: hidden, // Obscure if true.
          decoration: new InputDecoration(
            labelStyle: new TextStyle(
              color: Colors.white70,
              fontWeight: FontWeight.bold,
            ),
            labelText: label,
          ),
        ),
      ),
    ),
  );
}

Widget buttonContainer(Color buttonColor, String buttonText, double buttonFontSize){
  return Container(
      alignment: Alignment.center,
      height: 60.0,
      decoration: new BoxDecoration(
          color: buttonColor,
          borderRadius: new BorderRadius.circular(18.0)),
      child: new Text(buttonText,
          style: new TextStyle(fontSize: buttonFontSize, color: Colors.white))
  );
}
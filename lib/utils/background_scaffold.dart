import 'package:flutter/material.dart';
import 'package:stat_route/utils/user_utils.dart';

Widget scaffoldBackground(Widget child, GlobalKey<ScaffoldState> _scaffoldKey, String imageStr) {
//  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  return new Scaffold(
    key: _scaffoldKey,
    appBar: myAppBar(),
    body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(imageStr),
          fit: BoxFit.fill,
        ),
      ),
      child: child,
    ),
  );
}